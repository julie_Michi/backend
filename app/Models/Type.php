<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = [
        'label', 'description'
    ];
//gere relation plrs à plrs
    public function products() {
        return $this->hasMany(Product::class);
    }
}
