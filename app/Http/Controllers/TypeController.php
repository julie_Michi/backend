<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Type;

class TypeController extends Controller
{//all type
    public function show() {
        $types = Type::all();
        if($types->count() <= 0) {
            return response()->json('aucun type existant', 401);
        } else {
            return response()->json($types, 200);
        }
        
    }
//add type
    public function add(Request $request) {
        $this->validate($request, [
            'label' => ['required', 'max:15']
        ]);
        $type = new Type();
        $type->description = $request->description;
        // $type->image = $request->image;
        $type->fill($request->all());
        $type->save();

        return response()->json('type ajouté avec succès', 200);

    }

    public function modify(Request $request, $id) {
        $type = Type::findOrFail($id);
        $type->label = $request->label;
        $type->description = $request->description;
        
        $type->save();
        return response()->json('type est modifié avec succès', 200);
    }

    public function details($id) {
        $type = Type::where('id', '=', $id)->get();
        return response()->json($type, 200);
    }

    public function search($word) {
        $type= Type::where('label', '=', $word)->get();
        if($type->count() <= 0) {
            return response()->json('type n existe pas', 401);
        } else {
            return response()->json($type, 200);
        }
    }

    public function delete($id) {
        $type = Type::findOrFail($id);
        $type->delete();

        return response()->json('type supprimée avec succès', 200);
    }

   
}
