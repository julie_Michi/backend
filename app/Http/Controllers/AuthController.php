<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthController extends Controller
{
    public function __construct() {
        //exception de use case n'ayant pas besoin de token pr funct
        //ens de route 
        //return $this -> middleware('auth:api', ['except' => ['login', 'signup']]);
    }
    //s'inscrire
    public function signup(Request $request) {
        $this->validate($request,[
            'name' => 'required', 
            'email' => ['required', 'email'], 
            'phone' => ['required']
        ]);

        $user = new User();
        $user->sex = $request->sex;
        //hash password
        $user-> password = Hash::make($request->password);
        //retirer pas dans mn diagramme classe
        //$user -> image = $request -> image;

        $user->fill($request->all());
        $user->save();

        return response()->json('utilisateur créé avec succes', 200);

    }
    //loguer
    public function login(Request $request) {
        $this->validate($request,[
            'email' => ['required', 'email'],
            'password' => 'required'
        ]);
        $email = $request -> email;
        $password = $request -> password;
        //middleware en action avec auth
        $credentials = request(['email', 'password']);

        //attempt methode authentification lumen
        //attempt ne reconnait pas request dou credentials
        if(!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->generateToken($token);

    }
    //token generer
    // protected function generateToken($token) {
    //     //expired temps expiration
    //     return response()->json(['user' => auth()->$user,'acces_token' => $token,'token_type' => 'bearer', 'expired' => auth()->factory()->getTTL() * 60]);

    // }

    protected function generateToken($token)
    {
        return response()->json([
            'user' => auth()->user(),
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
    //logout
    public function logout() {
        auth()->logout();
        return response()->json('deconnecter avec succes', 200);
    }

    public function currentUser() {
        //qui est connecté
        auth()->user();
    }

    //refresh token
    public function refreshToken() {
        return $this->generateToken(auth()->refresh());
    }


}
