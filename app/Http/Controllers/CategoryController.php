<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    //getCategory
    public function display() {
        $categories = Category::all();
        if ($categories->count() <= 0){
            return response()->json('pas de categories disponibles', 401);
        } else {
            return response()->json($categories, 200);
        }
    }

    //specific category
    public function search($word) {
        $category = Category::where('label', '=', $word)->get();
        if($category->count() <= 0) {
            return response()->json('cette categorie n existe pas', 401);
        } else {
            return response()->json($category, 200);
        }
    }

    public function add(Request $request) {
        $this->validate($request,[
            'label' => ['required', 'max:15']
        ]);
        $category = new Category();
        $category->description = $request->description;
        $category->image = $request->image;
        //partie conditionee
        $category->fill($request->all());
        $category->save();

        return response()->json('categorie crée avec succès', 200);

    }

    public function modify(Request $request, $id) {
        $category = Category::findOrFail($id);
        $category->label = $request->label;
        $category->description = $request->description;
        $category->image = $request->image;
        $category->save();

        return response()->json('categorie modifiée avec succès', 200);


    }

    public function delete($id) {
        $category = Category::findOrFail($id);
        $category->delete();

        return response()->json('categorie supprimée avec succès', 200);
    }

    public function details($id) {
        $category = Category::where('id', '=', $id)->get();
        return response()->json($category, 200);

    }
}
