<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function display() {
        $products = Product::all()->order_by('name');
        return response()->json($products, 200);
    }

    public function add(Request $request) {
        $this->validate($request, [
            'name' => ['required', 'max:20'],
            'price' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            
            
        ]);
        $product = new Product();
        //genere auto
        $product->ref = Str::random(5);
        $product->qty = $request->qty;
        $product->type_id = $request->type_id;
        //part conditionné
        $product->fill($request->all());
        $product->save();
        if($product) {
            return response()->json('ajouter avec succes', 200);

        } else {
            return response()->json('produit n est pas enregistré', 401);
        }

    }

    public function modify(Request $request, $id) {
        $product = Product::findOrFail($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->qty = $request->qty;
        $product->save();
        return response()->json('produit modifié avec succes', 200);
    }

    public function search($word) {
        $product = new Product();
        $word = $product->category()->label;
         
    }
}
