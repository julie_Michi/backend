<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
//gerer prefix endpoint & middleware :addons 
$router->group(['prefix' => 'shopping'], function() use ($router) {
    $router->post('login', ['uses' => 'AuthController@login']);
    $router->post('adduser', ['uses' => 'AuthController@signup']); 
    
});

$router->group(['prefix' => 'shopping','middleware'=>'auth'], function() use ($router) {
    $router->get('categories/all', ['uses' => 'CategoryController@display']);
    $router->get('categories/{word}', ['uses' => 'CategoryController@search']);
    $router->get('category/{id}', ['uses' => 'CategoryController@details']);
    $router->post('category', ['uses' => 'CategoryController@add']);
    $router->put('category/{id}', ['uses' => 'CategoryController@modify']);
    $router->delete('category/{id}', ['uses' => 'CategoryController@delete']);

     //type
     $router->get('types/all', ['uses' => 'TypeController@show']);
     $router->get('type/{id}', ['uses' => 'TypeController@details']);
     $router->get('types/{word}', ['uses' => 'TypeController@search']);
     $router->post('type', ['uses' => 'TypeController@add']);
     $router->put('type/{id}', ['uses' => 'TypeController@modify']);
     $router->delete('type/{id}', ['uses' => 'TypeController@delete']);

      //user
    
   
    $router->post('/refresh', ['uses' => 'AuthController@refreshToken']);
    $router->post('/logout', ['uses' => 'AuthController@logout']);
    $router->post('/user', ['uses' => 'AuthController@currentUser']);


    //product
    $router->get('products/all', ['uses' => 'ProductController@display']);
    $router->get('products/{word}', ['uses' => 'ProductController@search']);
    $router->get('product/{id}', ['uses' => 'ProductController@details']);
    $router->post('product', ['uses' => 'ProductController@add']);
    $router->put('product/{id}', ['uses' => 'ProductController@modify']);
    $router->delete('product/{id}', ['uses' => 'ProductController@delete']);


});

